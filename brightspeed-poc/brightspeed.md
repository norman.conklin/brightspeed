**Spirent Velocity Automation Platform integrated with traffic generator and L1 switch for PON throughput automation test **
#

**Proof of Concept User Summary:**
 - User: Brightspeed
 - Access Dates: 10/23/23 – 11/3/2023
 - Access Times: 24H (user has access to testbed for 24h per day during access dates)
 - Access Level: Full (user can create, configure & run tests)

** Description: Test & Measurement Capabilities:**
In this testbed, we are emulating automation testing platform, using velocity to control traffic generator (STC) and DUT (PON OLT/Splitter/ONU, simulated by NetScout switch), creating topology (change connections), execute throughput automation test case and generate report. 

** POC Test criteria:**
 -	Abstracting resource: STC ports, OLT ports, Splitter/ONU ports
 -	Moving connections between STC-OLT-Splitter-ONU-STC, creating test topology
 -	Building throughput automation test case
 -	Generating test report
 -	Showing Automation platform functions - statistic/schedule/execution 

** POC Milestones:**
 -	Abstracting resources
 -	Creating test topology 
 -	Building throughput automation test case 
 -  Generating test report

** Challenges:**
 -	Abstract all Lab resources (Traffic Generator, DUT, L1/L2 switch etc.)
 -	Create test topology.
 -	Build automation test case.
 -	Control and execute test case.
 -	Report and integration 


** Customer Benefits:**
 -	Gain confidence that Velocity satisfies customer’s automation testing Lab requirement.
 -	Velocity can be integrated with customer environments.
 -	STC satisfied performance testing requirements.

** Solution Elements:**
 -	TestCenter 
 -	Velocity
 -	NetScout


----
----

#### Content

| Links |
| ------ |
| [TestCenter](https://www.spirent.com/products/testcenter-ethernet-ip-cloud-test) |
| [Velocity](https://www.spirent.com/products/lab-automation-velocity) |
| [Netscout](https://www.spirent.com/products/velocity-automation-switches) |

